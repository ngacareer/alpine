# Alpine Linux in Docker

[![Docker Automated build](https://img.shields.io/docker/automated/ngacareer/alpine.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/ngacareer/alpine/)
[![Docker Pulls](https://img.shields.io/docker/pulls/ngacareer/alpine.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/ngacareer/alpine/)
[![Docker Stars](https://img.shields.io/docker/stars/ngacareer/alpine.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/ngacareer/alpine/)

[![Alpine Version](https://img.shields.io/badge/Alpine%20version-v3.13.0-green.svg?style=for-the-badge&logo=alpine-linux)](https://alpinelinux.org/)

This Docker image [(ngacareer/alpine)](https://hub.docker.com/r/ngacareer/alpine/) is based on the minimal [Alpine Linux](https://alpinelinux.org/).

##### Alpine Version 3.13.0 (Released Jan 14, 2021)

This docker image is the base Alpine Linux. For more info on versions & support see [Releases](https://wiki.alpinelinux.org/wiki/Alpine_Linux:Releases)

----

## What is Alpine Linux?
Alpine Linux is an independent, non-commercial, general purpose Linux distribution designed for power users who appreciate security, simplicity and resource efficiency. Alpine Linux is built around musl libc and busybox, very simple distribution and security in mind. The image is only 5 MB . This makes Alpine Linux a great image base for utilities and even production applications. Read more about Alpine Linux here and you can see how their mantra fits in right at home with Docker images.

## Features

* Minimal size only, minimal layers
* Memory usage is minimal on a simple install.

## Architectures

* ```:amd64```, ```:x86_64``` - 64 bit Intel/AMD (x86_64/amd64)
* ```:arm64v8```, ```:aarch64``` - 64 bit ARM (ARMv8/aarch64)
* ```:arm32v7```, ```:armhf``` - 32 bit ARM (ARMv7/armhf)

#### PLEASE CHECK TAGS BELOW FOR SUPPORTED ARCHITECTURES, THE ABOVE IS A LIST OF EXPLANATION

## Tags

* ```:latest``` latest branch based (Automatic Architecture Selection)
* ```:amd64```, ```:x86_64``` amd64 based on latest tag but amd64 architecture
* ```:aarch64```, ```:arm64v8``` Armv8 based on latest tag but arm64 architecture
* ```:armhf```, ```:arm32v7``` Armv7 based on latest tag but arm architecture

## How to use this image
#### Usage
Use like you would any other base image:

```
FROM ngacareer/alpine
RUN apk add --no-cache mysql-client
ENTRYPOINT ["mysql"]
```

## Source Repositories

* [Github - ngacareer/alpine](https://github.com/ngacareer/alpine)

* [Gitlab - ngacareer/alpine](https://gitlab.com/ngacareer/alpine)

* [Bitbucket - ngacareer/alpine](https://bitbucket.org/ngacareer/alpine/)


## Container Registries

* [Dockerhub - ngacareer/alpine](https://hub.docker.com/r/ngacareer/alpine/)

* [Quay.io - ngacareer/alpine](https://quay.io/repository/ngacareer/alpine)


## Links

* [Ngacareer Systems](https://ngacareer.com/)

* [Github - Ngacareer Systems](https://github.com/ngacareer/)

* [Dockerhub - Ngacareer Systems](https://hub.docker.com/u/ngacareer/)

* [Quay.io - Ngacareer Systems](https://quay.io/organization/ngacareer)

## Donation

[![BMAC](https://img.shields.io/badge/BUY%20ME%20A%20COFFEE-$1-blue.svg?style=for-the-badge&logo=buy-me-a-coffee)](https://www.buymeacoffee.com/ngacareer?new=1)
